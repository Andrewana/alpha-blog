status = "peace"

buggy_logger = status

print "Status: "
print buggy_logger << "\n" # <- This insertion is the bug because it appends the string. '<<' means appends

def launch_nukes?(status)
  unless status == 'peace'
    return true
  else
    return false
  end
end

print "Nukes Launched: #{launch_nukes?(status)}\n"

# => Status: peace
# => Nukes Launched: true